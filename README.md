# Evento 24H24L

Los contenidos se publican con GitLab Pages en: https://vifito.gitlab.io/24h24l

## Construcción de la web

Para construír la web se emplea la imagen docker de AsciiDoc.

```bash
# Clonar repositorio
git clone https://gitlab.com/vifito/24h24l.git

# Directorio del proyecto
cd 24h24l

# Descargar imagen docker con AsciiDoctor (sólo la primera vez)
docker pull asciidoctor/docker-asciidoctor:1.1.0

# Lanzar el contenedor
docker run --name asciidoc -it -v `pwd`:/documents/ --rm asciidoctor/docker-asciidoctor:1.1.0

# Construír HTML y PDF
make

# Salir del contenedor
exit
```
