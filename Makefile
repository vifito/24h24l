SOURCE=./adoc/*.adoc

release: clean html

html:
	mkdir -p ./public/images ./public/audios ./public/assets
	cp -Rf ./adoc/images/* ./public/images/ 
	cp -Rf ./adoc/assets/* ./public/assets/
	#cp -Rf ./adoc/audios/* ./public/audios/
	asciidoctor -a linkcss --doctype article --source-dir ./adoc --destination-dir ./public $(SOURCE)

clean:
	rm -Rf ./public/*
